package rs.edu.raf.msa.pbp.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import rs.edu.raf.msa.pbp.model.Play;
import rs.edu.raf.msa.pbp.model.PlayByPlay;
import rs.edu.raf.msa.pbp.model.Player;

@SpringBootTest
@Slf4j
class GameControllerTest {

	@Autowired
	GameController gameController;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	void gameLoaded() throws IOException {
		PlayByPlay pbp = gameController.game("20200924LALDEN");
		assertNotNull(pbp);

		String formattedJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(pbp);
		log.debug(formattedJson);
	}

	@Test
	void gamesLoaded() throws IOException {
		List<String> games = gameController.games();
		assertNotNull(games);

		log.debug("{}", games);
		assertThat(games).contains("20200924LALDEN", "20200930MIALAL", "20201002MIALAL");
	}

	@Test
	void playsLoaded() throws IOException {
		List<Play> plays = gameController.plays("20200924LALDEN","11:50","12:30");
		assertEquals(7,plays.size());

		plays = gameController.plays("20200924LALDEN","00:01","00:03");
		assertEquals(2,plays.size());

		plays = gameController.plays("20200924LALDEN","23:59","24:10");
		assertEquals(3,plays.size());
	}

	@Test
	void playersMethodTest() throws IOException {
		List<Player> players = gameController.players("20200924LALDEN");
		assertEquals(34,players.size());
	}

}
