package rs.edu.raf.msa.pbp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Play {

   int p;

   @JsonProperty("c")
   String time;

   @JsonProperty("d")
   String description;

   @JsonProperty("t")
   String team;

   String atin;

   int x;

   int y;

   @JsonProperty("hs")
   int homeScore;

   int id;

   @JsonProperty("vs")
   int awayScore;

   String et;

   List<String> players;


}
