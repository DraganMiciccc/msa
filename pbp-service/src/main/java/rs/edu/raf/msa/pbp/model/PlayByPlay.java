package rs.edu.raf.msa.pbp.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class PlayByPlay {

	Map<String, Player> players = new LinkedHashMap<>();
	
	List<Quarter> quarters = new ArrayList<>(4);
	
	public List<Play> play(String fromMin, String toMin) {
		// TODO Returns all plays by all quarters whose
		return Collections.emptyList();
	}
	
}
