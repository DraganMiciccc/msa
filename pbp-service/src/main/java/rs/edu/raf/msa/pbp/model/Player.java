package rs.edu.raf.msa.pbp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Player {
	@JsonProperty("c")
	String fullName;
	@JsonProperty("f")
	String firstName;
	@JsonProperty("l")
	String lastName;

	String externalId;

	
}
