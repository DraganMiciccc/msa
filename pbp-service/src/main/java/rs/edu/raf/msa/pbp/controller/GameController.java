package rs.edu.raf.msa.pbp.controller;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.ObjectMapper;

import rs.edu.raf.msa.pbp.model.*;


@RestController
@Slf4j
public class GameController {

	@Autowired
	ObjectMapper objectMapper;

	@GetMapping("/game/{gameId}")
	public PlayByPlay game(@PathVariable String gameId)  {
		return plejevi(gameId);
	}
	
	@GetMapping("/games")
	public List<String> games() {
		try {

			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);

			Resource[] resources;
			resources = resolver.getResources("classpath:games/*.json");
			List<String> games = new ArrayList<>( resources.length);
			for (Resource r:
					resources) {
				games.add(Objects.requireNonNull(r.getFilename()).replace(".json",""));
			}
			return games;

		} catch (IOException e) {
			log.error("Neuspesno ucitavanje gejmova!");
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Neuspesno ucitavanje gejmova!");
		}
	}

	@GetMapping("/players/{gameId}")
	public List<Player> players(@PathVariable String gameId) {

		PlayByPlay playByPlay =  plejevi(gameId);
		List<Player> players = new ArrayList<>();

		for (Map.Entry<String,Player> entry: playByPlay.getPlayers().entrySet()){
			Player player = entry.getValue();
			player.setExternalId(entry.getKey());
			players.add(player);
		}
		return players;

	}

	@GetMapping("/plays/{gameId}/{fromMin}/{toMin}")
	public List<Play> plays(@PathVariable String gameId, @PathVariable String fromMin, @PathVariable String toMin) {

		List<Quarter> quarters = plejevi(gameId).getQuarters();
		LocalTime startTime = stingToTime(fromMin);
		LocalTime endTime = stingToTime(toMin);

		log.debug("Nema plejeva za: gameId: {}, start time: {}, end time: {}", gameId, startTime, endTime);
		List<Play> plays = new ArrayList<>();
		int timeOffset = 0;

		for (Quarter q : quarters) {
			for (Play play : q.getPlays()) {

				LocalTime time = stingToTime(play.getTime());
				time = time.plusMinutes((long) timeOffset * 12);

				if (comapreTime(time,startTime) > 0 && comapreTime(time,endTime) < 0){
					plays.add(play);
					log.debug("Uzet plej {} iz kvartera {}",play.getDescription(),q.getQ());
				}
			}
			timeOffset++;
		}
		return plays;
	}

	private PlayByPlay plejevi (String gameId){
		try{

			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream input = classLoader.getResourceAsStream("games/" + gameId + ".json");
			return objectMapper.readValue(input, PlayByPlay.class);

		} catch (IOException e){
			log.error("Could not load plays");
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error opening plays");
		}
	}

	private LocalTime stingToTime (String timeString){

		LocalTime time = null;
		String[] parsedString = timeString.split(":");

		int minute = 0;
		int second = 0;
		int nano = 0;

		if (parsedString.length != 2){
			log.error("Tried to parse invalid string: "+ timeString);

		}
		minute = Integer.parseInt(parsedString[0]);
		String rawSecond = parsedString[1];
		if (rawSecond.contains(".")){
			String[] splitSecond = rawSecond.split("\\.");
			second = Integer.parseInt(splitSecond[0]);
			nano = Integer.parseInt(splitSecond[1]);
		} else {
			second = Integer.parseInt(rawSecond);
		}

		log.debug("Should parse time minutes: {}, seconds: {}, nano: {}",minute,second,nano);
		time = LocalTime.of(0,minute,second,nano);
		return time;
	}

	private int comapreTime(LocalTime a, LocalTime b){
		return a.compareTo(b);
	}
}
