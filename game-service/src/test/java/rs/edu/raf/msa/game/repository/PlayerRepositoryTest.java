package rs.edu.raf.msa.game.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

import lombok.extern.slf4j.Slf4j;
import rs.edu.raf.msa.game.entity.Game;
import rs.edu.raf.msa.game.entity.Play;
import rs.edu.raf.msa.game.entity.Player;

@SpringBootTest
@ActiveProfiles("test")
@Slf4j
@Transactional
public class PlayerRepositoryTest {

	@Autowired
	GameRepository gameRepository;

	@Autowired
	PlayerRepository playerRepository;

	@Autowired
	PlayRepository playRepository;

	@Test
	void testAll() {
		Game g = Game.builder().code("20200924LALDEN").homeTeam("LAL").visitorTeam("DEN").finished(false).build();
		Game savedGame = gameRepository.save(g);

		assertEquals(g.getCode(),savedGame.getCode());

	}

	@Test
	void savePlayer() {
		Player p = Player.builder().externalId(2544).firstName("LeBron").lastName("James").code("lebron_james").build();
		Player savedPlayer = playerRepository.save(p);

		assertEquals(p.getCode(),savedPlayer.getCode());

		Player foundPlayer = playerRepository.findByCode("lebron_james");

		assertEquals(p.getCode(),foundPlayer.getCode());
	}

	@Test
	void savePlay() {
		Game g = Game.builder().code("20200924LALDEN").homeTeam("LAL").visitorTeam("DEN").build();
		g = gameRepository.save(g);
		Play p = Play.builder().gameId(g.getId()).externalId(2).description("Start Period").team("LAL").time("12:00").build();
		Play savedPlay = playRepository.save(p);


		assertEquals(p.getId(),savedPlay.getId());
		assertEquals(p.getExternalId(),savedPlay.getExternalId());
		assertEquals(g.getId(),savedPlay.getGameId());
	}

	@Test
	void findByExternalId() {
		Player p = Player.builder().externalId(2544).firstName("LeBron").lastName("James").code("lebron_james").build();
		playerRepository.save(p);
		Player foundPlayer = playerRepository.findByExternalId(2544);

		assertEquals(p.getId(),foundPlayer.getId());
	}
}
