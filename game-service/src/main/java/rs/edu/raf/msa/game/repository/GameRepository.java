package rs.edu.raf.msa.game.repository;

import org.springframework.data.annotation.Id;
import org.springframework.data.repository.PagingAndSortingRepository;

import rs.edu.raf.msa.game.entity.Game;

import java.util.Optional;


public interface GameRepository extends PagingAndSortingRepository<Game, Id> {
    Game findByCode(String code);

    Game findById(Long gameId);
}
