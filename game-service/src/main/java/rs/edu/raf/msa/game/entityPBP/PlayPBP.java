package rs.edu.raf.msa.game.entityPBP;

import lombok.Data;

import java.util.List;


@Data
public class PlayPBP {

    int p;
    String d;
    String t;
    int id;
    String c;

    int vs;
    int hs;

    String atin;


    List<String> players;
	
}
