package rs.edu.raf.msa.game.entityPBP;

import lombok.Data;

@Data
public class PlayerPBP {

    String c;

    String f;

    String l;

    String externalId;
}
