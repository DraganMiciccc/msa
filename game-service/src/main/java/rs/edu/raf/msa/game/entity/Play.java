package rs.edu.raf.msa.game.entity;

import lombok.Builder;
import org.springframework.data.annotation.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@Builder
public class Play {

	@Id
	Long id;

	int externalId;


	String description;

	String time;

	String team;

	Long gameId;

	int homeScore;

	int awayScore;

	String gameTime;

	int points;

	String players;


	public static String serializePlayers(List<String> players){
		if (players == null){
			return "";
		}
		return String.join(",",players);
	}

	public List<String> razdvojPlejere(){
		return Arrays.asList(this.players.split(","));
	}
}
