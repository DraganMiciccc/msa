package rs.edu.raf.msa.game.entity;

import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.relational.core.mapping.MappedCollection;

import java.util.Set;

@Data
@Builder
public class Game {

	@Id
	Long id;

	String code;

	String homeTeam;

	String visitorTeam;

	boolean finished;

	@MappedCollection(keyColumn = "game_id",idColumn = "id")
	private Set<Play> plays;

	int lastParsed;

	
}
