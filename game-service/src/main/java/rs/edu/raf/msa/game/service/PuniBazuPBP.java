package rs.edu.raf.msa.game.service;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import rs.edu.raf.msa.game.GameClient;
import rs.edu.raf.msa.game.entityPBP.PlayPBP;
import rs.edu.raf.msa.game.entityPBP.PlayerPBP;
import rs.edu.raf.msa.game.entity.Game;
import rs.edu.raf.msa.game.entity.Play;
import rs.edu.raf.msa.game.entity.Player;
import rs.edu.raf.msa.game.repository.GameRepository;
import rs.edu.raf.msa.game.repository.PlayRepository;
import rs.edu.raf.msa.game.repository.PlayerRepository;

@Component
@RequiredArgsConstructor
@Slf4j
public class PuniBazuPBP {

    private  int gameProcesInterval = 10;
    private  int duzinaGejma = 50;

    final GameClient gameClient;
    final PlayerRepository playerRepository;
    final GameRepository gameRepository;
    final PlayRepository playRepository;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("mm:ss");

    @Scheduled(fixedDelay = 3_000)
    public void gledajGames() {

        List<String> allGames = gameClient.games();
        log.info("Ucitavam gejmove sa pbp-servisa: {}", allGames);

        for (String gameId: allGames) {

            Game game = gameRepository.findByCode(gameId);

            if (game == null){   //ako je gejm 0 pravim ga ////////////////////////////////

                String teams = gameId.substring(gameId.length() - 6 );
                String homeTeam = teams.substring(0,3);
                String visitorTeam = teams.substring(3,6);
                Game g = Game.builder()
                        .code(gameId)
                        .finished(false)
                        .homeTeam(homeTeam)
                        .visitorTeam(visitorTeam)
                        .build();
                game = gameRepository.save(g);

            }
            if (game.isFinished()){        //ako je zavrsen idem dalje //////////////////////////////////////////
                continue;
            }
            if (!game.isFinished() && game.getLastParsed() == 0){    //ako nije zavrsen obradjujem plejere     /////////////////////////////////

                List<PlayerPBP> players = gameClient.players(gameId);
                log.info("Obradjujem igrace iz igre {}",gameId);

                for(PlayerPBP data : players){

                    long externalId = Long.parseLong(data.getExternalId());
                    Player player = playerRepository.findByExternalId(externalId);
                    if (player == null){
                        player = Player.builder().externalId(Math.toIntExact(externalId)).firstName(data.getF()).lastName(data.getL()).code(data.getC()).build();
                        log.info("Cuvam igraca {}",player.getCode());
                        playerRepository.save(player);
                    } else {
                        log.info("Igrac {} vec postoji",player.getCode());
                    }
                }
            }
            LocalTime start = LocalTime.of(0, 0, 0);
            start = start.plusMinutes(game.getLastParsed());
            LocalTime end = start.plusMinutes(gameProcesInterval);

            String startTimeString = start.plusMinutes(game.getLastParsed()).format(formatter);
            String endTimeString = start.plusMinutes(gameProcesInterval).format(formatter);

            List<PlayPBP> plays = gameClient.plays(game.getCode(),startTimeString, endTimeString);
            log.info("{} plejeva za parsiranje u igri {} u intervalu - start: {} end: {}",plays.size(),game.getCode(),startTimeString,endTimeString);
            for (PlayPBP playDto: plays){
                Play play = Play.builder()
                        .gameId(game.getId())
                        .externalId(playDto.getId())
                        .team(playDto.getT())
                        .description(playDto.getD())
                        .time(playDto.getC())
                        .homeScore(playDto.getHs())
                        .awayScore(playDto.getVs())
                        .gameTime(playDto.getAtin())
                        .points(playDto.getP())
                        .players(Play.serializePlayers(playDto.getPlayers()))
                        .build();
                play = playRepository.save(play);
                log.info("Plej {} sacuvan",play.getDescription());
            }
            game.setLastParsed(end.getMinute());
            if (end.getMinute() >= duzinaGejma){
                game.setFinished(true);
            }
            gameRepository.save(game);
            log.info("Igra {} osvezena svim plejevima do {} minuta",game.getCode(),game.getLastParsed());
        }
    }

}
