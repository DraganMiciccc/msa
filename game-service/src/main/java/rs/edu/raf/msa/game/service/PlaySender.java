package rs.edu.raf.msa.game.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import rs.edu.raf.msa.game.config.MessagingConfig;
import rs.edu.raf.msa.game.entity.Game;
import rs.edu.raf.msa.game.entity.Play;
import rs.edu.raf.msa.game.entity.Player;
import rs.edu.raf.msa.game.message.GameScore;
import rs.edu.raf.msa.game.message.PlayerScore;
import rs.edu.raf.msa.game.repository.GameRepository;
import rs.edu.raf.msa.game.repository.PlayerRepository;

@RequiredArgsConstructor
@Component
@Slf4j
public class PlaySender {

    final RabbitTemplate rabbitTemplate;
    final GameRepository gameRepository;

    final PlayerRepository playerRepository;


    public void sendGameScore(Play p) {
        Game game = gameRepository.findById(p.getGameId());
        if (game == null) {
             throw new IllegalStateException("igra za plej: " + p.getId() + "nije pronadjena!");
        }

        GameScore gs = GameScore.builder()
                .gameId(game.getId())
                .gameCode(game.getCode())
                .homeTeam(game.getHomeTeam())
                .awayTeam(game.getVisitorTeam())
                .homeScore(p.getHomeScore())
                .awayScore(p.getAwayScore())
                .gameTime(p.getGameTime())
                .build();
        log.info("Poslat play {} na game-score ",p.getId());

        rabbitTemplate.convertAndSend("game-score", null, gs);
    }

    public void sendPlayerScore(Play p) {
        for (String playerId : p.razdvojPlejere()){
            if (playerId == null || playerId.length() == 0){
                return;
            }
            Player player = playerRepository.findByExternalId(Long.parseLong(playerId));
            if (player == null){
                continue;
            }
            if (p.getPoints() == 0){
                continue;
            }
            String playerName = player.getFirstName() + " " + player.getLastName();
            log.info("Poslat player score za za igraca {} na player-score-exchange ",playerName);
            PlayerScore ps = PlayerScore.builder()
                    .gameId(p.getGameId())
                    .player(playerName)
                    .points(p.getPoints())
                    .build();
            rabbitTemplate.convertAndSend("player-score-exchange", null, ps);
        }

    }

}
