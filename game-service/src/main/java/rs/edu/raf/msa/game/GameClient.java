package rs.edu.raf.msa.game;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.edu.raf.msa.game.entityPBP.PlayPBP;
import rs.edu.raf.msa.game.entityPBP.PlayerPBP;

@FeignClient(value = "gameClient", url = "http://localhost:8080/")
public interface GameClient {


	@RequestMapping(method = RequestMethod.GET, value = "/games")
	public List<String> games();

	@RequestMapping(method = RequestMethod.GET, value = "/players/{gameId}")
	public List<PlayerPBP> players(@PathVariable String gameId);

	@RequestMapping(method = RequestMethod.GET, value = "/plays/{gameId}/{fromMin}/{toMin}")
	public List<PlayPBP> plays(@PathVariable String gameId, @PathVariable String fromMin, @PathVariable String toMin);

}
