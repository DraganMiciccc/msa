drop table if exists player;
drop table if exists play;
drop table if exists game;

create table player(
	id serial not null primary key,

	external_id int not null unique,

    code varchar(50) not null unique,

    first_name varchar(20) not null,
    last_name varchar(20) not null
);

create table game(
	id serial not null primary key,
	code varchar(20) not null unique,

    home_team varchar(20) not null,
    visitor_team varchar(20) not null,

    finished boolean default false,
    last_parsed int not null default 0
);


create table play(
	id serial not null primary key,
    external_id int not null,
	time varchar(50) not null,
	description varchar(255) not null,
	team varchar(20) not null,
    home_score int not null,
    away_score int not null,
    points int not null,
    game_time varchar(50) default null,
    players varchar(255) default null,

	game_id int not null references game
);
