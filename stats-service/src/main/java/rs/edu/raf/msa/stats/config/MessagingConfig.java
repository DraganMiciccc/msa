package rs.edu.raf.msa.stats.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig {

	public static final String GAME_SCORE_EXCHANGE = "game-score";
	public static final String PLAYER_SCORE_EXCHANGE = "player-score-exchange";
	public static final String GAME_SCORE_RUN = "game-score.run";
	public static final String GAME_SCORE_DROUGHT = "game-score.drought";

	public static final String PLAYER_SCORE = "player-score";

	@Bean
	public Declarables gameScoreBindings() {
		FanoutExchange exchange = gameScoreExchange();
		FanoutExchange playerExchange = playerScoreExchange();

		return new Declarables(
				exchange,
				BindingBuilder.bind(playerScore()).to(playerExchange),
				BindingBuilder.bind(gameScoreRun()).to(exchange),
				BindingBuilder.bind(gameScoreDrought()).to(exchange)
		);
	}

	@Bean
	public Queue playerScore() {
		return QueueBuilder.durable(PLAYER_SCORE).build();
	}

	@Bean
	public Queue gameScoreRun(){
		return QueueBuilder.durable(GAME_SCORE_RUN).build();
	}
	@Bean
	public Queue gameScoreDrought(){
		return QueueBuilder.durable(GAME_SCORE_DROUGHT).build();
	}

	@Bean
	public FanoutExchange gameScoreExchange(){
		return new FanoutExchange(GAME_SCORE_EXCHANGE);
	}
	@Bean
	public FanoutExchange playerScoreExchange(){
		return new FanoutExchange(PLAYER_SCORE_EXCHANGE);
	}

	@Bean
	public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
	    return new Jackson2JsonMessageConverter();
	}
}
