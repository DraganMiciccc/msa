package rs.edu.raf.msa.stats.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import rs.edu.raf.msa.stats.config.MessagingConfig;
import rs.edu.raf.msa.stats.message.GameScore;
import rs.edu.raf.msa.stats.message.PlayerScore;
//import rs.edu.raf.msa.stats.model.Drought;
//import rs.edu.raf.msa.stats.model.Stats;
//import rs.edu.raf.msa.stats.model.Team;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Map;

@Service
@Slf4j
public class StatsService {

    @Autowired
    Map<String, Integer> playerPointsStore;



    @RabbitListener(queues = MessagingConfig.PLAYER_SCORE)
    public void playerScoreChanged(Message<PlayerScore> message) {

        System.out.println("SSSSSSSSSSSSSSSSSSSSS");
        PlayerScore ps = message.getPayload();
        String key = ps.getPlayer();
        if (key == null){
            log.error("PlayerScore key is null, {}",ps);
            return;
        }
        if (!playerPointsStore.containsKey(key)){
            playerPointsStore.put(key,0);
        }
        Integer currentPoints = playerPointsStore.get(key);
        playerPointsStore.put(key, currentPoints + ps.getPoints());
        System.out.println("playerScoreChanged(): Player {} scored {} points" + ps.getPlayer() + ps.getPoints());
        log.info("playerScoreChanged(): Player {} scored {} points", ps.getPlayer(),ps.getPoints());

    }

}
