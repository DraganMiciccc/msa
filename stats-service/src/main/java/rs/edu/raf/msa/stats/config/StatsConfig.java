package rs.edu.raf.msa.stats.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class StatsConfig {

    @Bean
    public Map<String, Integer> playerPointsStore(){
        return new ConcurrentHashMap<>();
    }

}