package rs.edu.raf.msa.stats.message;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GameScore {

    Long gameId;
    String gameCode;
    //hs
    int homeScore;
    //vs
    int awayScore;

    String homeTeam;
    String awayTeam;

    String gameTime;

    String team;

    int points;
}